terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
   # Backend wordt gebruikt voor authenticatie, opslag, vergrendeling en state-encryptie.
   backend "http" {
    address = "https://gitlab.com/api/v4/projects/48178246/terraform/state/default"
    lock_address = "https://gitlab.com/api/v4/projects/48178246/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/48178246/terraform/state/default/lock"
    }
}

provider "aws" {
  region = "us-east-1"
} 



# Hieroner worden er Elastic Beanstalk binnne AWS , Load Balancing net als AutoScaling geconfigureerd en gemaakt.
resource "aws_elastic_beanstalk_application" "eb_app" {
  name = var.eb_app
} 
resource "aws_elastic_beanstalk_environment" "eb_env" {
  name                = var.eb_env
  application         = aws_elastic_beanstalk_application.eb_app.name
  solution_stack_name = var.solution_stack_name
  tier                = var.tier
 
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = var.vpc_id
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     =  "LabInstanceProfile"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     =  "True"
  }
 
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = join(",", var.public_subnets)
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "MatcherHTTPCode"
    value     = "200"
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "application"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "t3.small"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "internet facing"
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = 1
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = 3
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "Cooldown"
    value     = "500"
  }
  setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name      = "SystemType"
    value     = "enhanced"
  }
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "LoadBalancerHTTPSPort"
    value     = "443"
  }
 
}