# CI/CD Variables
## De volgende "Variables" moeten toegevoegd worden aan CI/CD in Settings

+ AWS_ACCESS_KEY_ID --> Access key id van AWSAWS_DEFAULT_REGION --> Default regio waar de applicatie zal draaien
+ AWS_SESSION_TOKEN --> Session token  van AWS 
+ AWS_S3_BUCKET --> De naam van S3 Bucket in AWS
+ AWS_SECRET_ACCESS_KEY --> Secret Access key id van AWS
+ GITLAB_DEPLOY_TOKEN --> In "Settings > Repository" open "Deploy tokens" en maak nieuwe token
+ EB_APP_NAME --> Applicatie Naam
+ EB_ENV_NAME --> De naam van environment 
+ PASS --> GITLab wachtwoord voor Container Registery
+ USERNAME --> GITLab username voor Container Registery