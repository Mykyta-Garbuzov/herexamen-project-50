FROM node:20-alpine3.17
COPY /game /usr/src/app/game
WORKDIR /usr/src/app/game
RUN apk update && apk add git
EXPOSE 80
RUN apk add --no-cache python3 g++ make
RUN yarn global add node-gyp
RUN yarn install
CMD ["yarn","run","build"] 
CMD ["yarn","run","start","--port", "80"] 
