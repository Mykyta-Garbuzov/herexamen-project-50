#Hieronder kunt u alle variabels terugvinden die Elastic Beanstalk gebruikt tijdens de configuratie.
variable "eb_app" {
  default = "projectapp"
}
variable "eb_env" {
  default = "projectenv"
}
variable "solution_stack_name" {
  default = "64bit Amazon Linux 2023 v4.0.0 running Docker"
}
variable "tier" {
  default = "WebServer"
}
variable "vpc_id" {
    default = "vpc-0c43666573b12b9a4"
}
variable "public_subnets" {
    default = ["subnet-041b7c4d39aad9e7e", "subnet-0ebaabd9650b6718e"]
}